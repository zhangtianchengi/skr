//获取用户信息
export const getinfo = {
  url: "/dev-api/getInfo",
  method: "get",
};
//专业
export const selectMajorStationList = {
  url: "/dev-api/sxpt/label/selectMajorStationList",
  method: "get",
};
//行业
export const selectTradeList = {
  url: "/dev-api/sxpt/label/selectTradeList",
  method: "get",
};
//行业
export const blackList = {
  url: "/dev-api/sxpt/blacking/blackList",
  method: "get",
};
//表格
export const selectProjectList = {
  url: "/dev-api/sxpt/project/selectProjectList",
  method: "get",
};
