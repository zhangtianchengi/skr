// 验证码
export const getcaptchaImage = {
  url: "/dev-api/captchaImage",
  method: "get",
};

// 登录
export const login = {
  url: "/dev-api/login",
  method: "post",
};

// 路由信息
export const getRouters = {
  url: "/dev-api/getRouters",
  method: "get",
};
