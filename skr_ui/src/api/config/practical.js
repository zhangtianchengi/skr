//计划页面请求
export const getPlanList = {
  url: "/dev-api/sxpt/classPlan/getPlanList",
  method: "get",
};

export const getClassInfo = {
  url: "/dev-api/sxpt/classPlan/getClassInfo",
  method: "get",
};

//进度页面下拉框数据
export const selectClassPlan = {
  url: "/dev-api/sxpt/progress/selectClassPlan",
  method: "get",
};

//进度页面小组数据
export const selectClassPlanInit = {
  url: "/dev-api/sxpt/progress/selectClassPlanInit",
  method: "get",
};

//进度排行数据
export const classRank = {
  url: "/dev-api/sxpt/progress/classRank",
  method: "get",
};

//答辩头部数据接口
export const selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};

//答辩表格数据接口
export const getDefenceList = {
  url: "/dev-api/sxpt/defence/getDefenceList",
  method: "get",
};
