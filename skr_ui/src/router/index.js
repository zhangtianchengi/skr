import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "Login",

    meta: {
      title: "八维教育",
    },
    component: () => import("../views/login/index.vue"),
  },

  {
    path: "/teacher",
    name: "teacher",
    component: () => import("../views/Teacher/Teacher.vue"),
    children: [
      {
        path: "/teacher",
        redirect: "/teacher/job",
      },
      {
        path: "/teacher/job",
        name: "job",
        meta: {
          title: "岗位 / 岗位管理",
        },
        component: () => import("../views/Teacher/Job/Job.vue"),
      },
      // 项目
      {
        path: "/teacher/project",
        name: "project",
        meta: {
          title: "项目 / 项目管理",
        },
        component: () => import("../views/Teacher/Project/Project.vue"),
      },
      // 实训
      {
        path: "/teacher/training",
        name: "training",
        meta: {
          title: "计划 / 计划管理",
        },
        component: () => import("../views/Teacher/Training/Training.vue"),
      },
      //计划
      {
        path: "/teacher/trainingPlan",
        name: "trainingPlan",
        component: () =>
          import("../views/Teacher/TrainingPlan/TrainingPlan.vue"),
      },
      //进度
      {
        path: "/teacher/schedule",
        name: "schedule",
        meta: {
          title: "进度 / 进度管理",
        },
        component: () => import("../views/Teacher/Schedule/Schedule.vue"),
      },
      //答辩
      {
        path: "/teacher/reply",
        name: "reply",
        meta: {
          title: "答辩 / 答辩管理",
        },
        component: () => import("../views/Teacher/Reply/Reply.vue"),
      },
      //计划管理
      {
        path: "/teacher/planManagement",
        name: "planManagement",
        meta: {
          title: "计划 / 计划管理",
        },
        component: () =>
          import("../views/Teacher/PlanManagement/PlanManagement.vue"),
      },
      //发起答辩
      {
        path: "/teacher/adddefence",
        name: "Adddefence",
        meta: {
          title: "答辩 / 发起答辩",
        },
        component: () => import("../views/Teacher/AddDefence/addDefence.vue"),
      },
      // 面试
      {
        path: "/teacher/interview",
        name: "interview",
        meta: {
          title: "面试",
        },
        component: () => import("../views/Teacher/Interview/Interview.vue"),
      },
      //面试记录
      {
        path: "/teacher/interviewRecord",
        name: "interviewRecord",
        meta: {
          title: "面试 / 面试记录",
        },
        component: () =>
          import("../views/Teacher/InterviewRecord/InterviewRecord.vue"),
      },
      //面试记录管理
      {
        path: "/teacher/interviewLeader",
        name: "interviewLeader",
        meta: {
          title: "面试 / 面试记录管理",
        },
        component: () =>
          import("../views/Teacher/InterviewLeader/InterviewLeader.vue"),
      },
      //面试排行榜
      {
        path: "/teacher/interviewRank",
        name: "interviewRank",
        meta: {
          title: "面试 / 面试排行管理",
        },
        component: () =>
          import("../views/Teacher/InterviewRank/InterviewRank.vue"),
      },
      // 问答
      {
        path: "/teacher/session",
        name: "session",
        meta: {
          title: "问答",
        },
        component: () => import("../views/Teacher/Session/Session.vue"),
      },
      //问答列表
      {
        path: "/teacher/answerList",
        name: "answerList",
        meta: {
          title: "问答列表",
        },
        component: () => import("../views/Teacher/AnswerList/AnswerList.vue"),
      },
      //问答管理
      {
        path: "/teacher/answerLeader",
        name: "answerLeader",
        meta: {
          title: "问答管理",
        },
        component: () =>
          import("../views/Teacher/AnswerLeader/AnswerLeader.vue"),
      },
      {
        path: "/teacher/addproject",
        meta: {
          title: "添加项目",
        },
        name: "AddProject",
        component: () => import("../views/Teacher/AddProject/AddProject.vue"),
      },
      {
        path: "/teacher/editproject",
        meta: {
          title: "编辑项目",
        },
        name: "EditProject",
        component: () => import("../views/Teacher/Edit/Edit.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
const writeList = ["/login"];
router.beforeEach((to, from, next) => {
  let token = window.localStorage.Authorization || "";
  if (writeList.includes(to.path)) {
    next();
  } else if (!token) {
    next("/login");
  }
  // 鉴权处理
  let f = from.path.split("/")[1];
  let t = to.path.split("/")[1];
  if (f !== t && f !== "login" && f) {
    next("/login");
  } else {
    next();
  }
});
export default router;
