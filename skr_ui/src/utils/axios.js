import axios from "axios";
import code from "./httpCode";
import { ElMessage } from "element-plus";

const Axios = axios.create({
  baseURL: "http://111.203.59.61:8060",
  timeout: 100000,
});

// 添加请求拦截器
Axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: localStorage.getItem("Authorization") || "",
      },
    };
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
Axios.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    const { data } = response;
    if (data.code == 200) {
      return Promise.resolve(response);
    } else {
      ElMessage.error(data.msg);
      return Promise.reject(response);
    }
  },
  function (error) {
    // 对响应错误做点什么
    console.dir(error);
    switch (error.code) {
      case "ECONNABORTED":
        ElMessage.error("请求超时");
        break;
    }
    code[error.code] && ElMessage.error(code[error.code]);
    return Promise.reject(error);
  }
);
export default Axios;
